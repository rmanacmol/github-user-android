# MVVM ~ Android Architecture Components ~ Kotlin
--------------------------------------------------

[![Build Status](https://app.bitrise.io/app/fc6cddc3b2fdee58/status.svg?token=j7yLI-wjQwayFw8Evp-lwA)](https://app.bitrise.io/app/fc6cddc3b2fdee58)

* [Kotlin](https://kotlinlang.org/)
* [Room](https://developer.android.com/topic/libraries/architecture/room.html)
* [ViewModels](https://developer.android.com/reference/android/arch/lifecycle/ViewModel.html)
* [LiveData](https://developer.android.com/reference/android/arch/lifecycle/LiveData.html)

Other libraries used
--------------------------

* [Dagger2](https://github.com/google/dagger)
* [RxJava](https://github.com/ReactiveX/RxJava)
* [Retrofit](https://github.com/square/retrofit)
* [OkHttp](http://square.github.io/okhttp/)
* [Glide](https://github.com/bumptech/glide)
* [Timber](https://github.com/JakeWharton/timber)

Architecture Components
--------------------------
![alt text](https://bitbucket.org/rmanacmol/assets/raw/f9e5c2676b7bf9e01cdced5ea656c4c6bc9d57b2/aac.png)

Screenshot
--------------------------
![](https://bitbucket.org/rmanacmol/assets/raw/539445e1d01f83f8eb81f89b505fd7d0ffee558c/mvmm-s.png)

Contribution
--------------------------

Feel free add feature or report any issues, I will be glad to improve it with your help.

Developed By
------------

* Renz Manacmol ~ manacmol@gmail.com


License
-------

Copyright 2018 Renz Manacmol

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.